module gitlab.com/osaki-lab/tagscanner

go 1.16

require (
	github.com/Songmu/gocredits v0.2.0
	github.com/go-chi/chi/v5 v5.0.2
	github.com/sergi/go-diff v1.2.0
	github.com/stretchr/testify v1.7.0
)
